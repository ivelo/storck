from .models import WorkspaceToken


def users_to_json(workspace_users):
    users = []
    for user in workspace_users.all():
        users.append({"id": user.id, "username": user.username})
    return users


def workspace_to_json(workspace, workspace_tokens=[]):
    return {
        "id": workspace.id,
        "name": workspace.name,
        "users": users_to_json(workspace.users),
        "tokens": list(workspace_tokens),
    }
