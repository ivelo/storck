from builtins import staticmethod, list
import uuid

from rest_framework import views
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from .models import Workspace, WorkspaceToken, MetadataSchemas
from .serializers import MetadataSchemaSerializer
from workspace.utils import workspace_to_json
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated

import json
from jsonschema import Draft202012Validator
import jsonschema


class CreateWorkspaceView(views.APIView):
    @staticmethod
    def post(request):
        post_data = request.POST
        workspace_name = post_data.get("name")

        token = request.headers.get('Authorization').replace('Token ', '')
        user = Token.objects.get(key=token).user

        workspace_token = str(uuid.uuid4())
        workspace_token_name = "Default token"

        workspace = Workspace.objects.create(name=workspace_name)
        WorkspaceToken.objects.create(
            workspace=workspace, name=workspace_token_name, token=workspace_token
        )

        workspace.save()
        workspace.users.add(user)

        workspace_tokens = WorkspaceToken.objects.filter(workspace=workspace).values()
        response = workspace_to_json(workspace, list(workspace_tokens))

        return Response({"data": response})


class GetUserWorkspacesView(views.APIView):
    @staticmethod
    def get(request):
        token = request.headers.get('Authorization').replace('Token ', '')
        user = Token.objects.get(key=token).user

        response = {"id": user.pk, "workspaces": []}

        user_workspaces = Workspace.objects.filter(users=user)

        for workspace in user_workspaces:
            workspace_tokens = WorkspaceToken.objects.filter(
                workspace=workspace
            ).values()

            response["workspaces"].append(
                workspace_to_json(workspace, list(workspace_tokens))
            )

        return Response({"data": response})


class AddUserToWorkspace(views.APIView):
    @staticmethod
    def post(request):
        post_data = request.POST
        workspace_token = post_data.get("workspace_token")
        new_user_id = post_data.get("user_id")
        new_user_name = post_data.get("user_name")


        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        new_user = User.objects.get(id=new_user_id) if new_user_id else User.objects.get(username=new_user_name)

        workspace.users.add(new_user)
        workspace.save()

        response = workspace_to_json(workspace)
        return Response({"data": response})

class SchemaView(views.APIView):

    @permission_classes([IsAuthenticated])
    @staticmethod
    def post(request):
        workspace_token = request.GET.get("token", None)
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        workspace_is_users = Workspace.objects.filter(pk=workspace.pk, users__pk=request.user.pk).exists()
        if not workspace_is_users:
            return Response(status=403, data={"message": "workspace access denied"})
        if (filetype := request.POST.get("filetype", None)) is None:
            return Response(status=400, data={'message': "filetype field is empty"})
        if (schema:= request.POST.get("metadata_schema", None)) is None:
            return Response(status=400, data={'message': "metadata_schema field is empty"})
        try:
            schema_j = json.loads(schema)
            schema_j["$schema"] = "file://../../schema"
            Draft202012Validator.check_schema(schema_j)
            res, created = MetadataSchemas.objects.update_or_create(
                filetype=filetype,
                workspace=workspace,
                # schema = schema_j,
                defaults = {'schema':schema_j, 'user':request.user}
            )
            operation = "created" if created else "updated"
            return Response(status=200, data={"id":res.id, "operation": operation})
        except json.JSONDecodeError:
            return Response(status=400, data={"message": "error when parsing JSON schema"})
        except jsonschema.exceptions.SchemaError as err:
            msg = str(err)
            return Response(status=400, data={"message": "invalid schema format: \n {}".format(msg)})


    @permission_classes([IsAuthenticated])
    @staticmethod
    def get(request):
        workspace_token = request.GET.get("token", None)
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        workspace_is_users = Workspace.objects.filter(pk=workspace.pk, users__pk=request.user.pk).exists()
        if not workspace_is_users:
            return Response(status=403, data={"message": "workspace access denied"})
        else:
            data = MetadataSchemas.objects.filter(workspace=workspace)
            if (filetype:= request.GET.get("filetype", False)):
                data = data.filter(filetype=filetype)
            ser = MetadataSchemaSerializer(data, many=True)
            res = {"schemas":ser.data}
            return Response(res)
