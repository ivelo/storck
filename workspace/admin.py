from django.contrib import admin
from .models import Workspace, WorkspaceToken, MetadataSchemas
from simple_history.admin import SimpleHistoryAdmin

admin.site.register(Workspace)
admin.site.register(WorkspaceToken)
admin.site.register(MetadataSchemas, SimpleHistoryAdmin)
