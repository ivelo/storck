from django.urls import path

from .views import VerifyTokenView, AuthenticateView

urlpatterns = [
    path('auth', VerifyTokenView.as_view(), name='api_token_verify'),
    path('authenticate', AuthenticateView.as_view(), name='api_authenticate')
]
