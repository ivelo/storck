import requests

from builtins import staticmethod
from rest_framework import views
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


def token_response(token, user):
    return {
        "token": token.key,
        "user_id": user.pk,
        "email": user.email,
        "username": user.username,
    }


class VerifyTokenView(views.APIView):
    @staticmethod
    def post(request):
        token = request.META['HTTP_AUTHORIZATION'].split()[1]
        token = Token.objects.get(key=token)
        user = User.objects.get(username=token.user)
        return Response(status=200, data=token_response(token, user))


class AuthenticateView(views.APIView):
    authentication_classes = []
    permission_classes = []
    
    @staticmethod
    def post(request):
        try:
            jwt_token = request.META['HTTP_AUTHORIZATION'].split()[1]
            user = authenticate(request, token=jwt_token) 
            if user and user.is_active:
                token = Token.objects.get(user=user)
                return Response(status=200, data=token_response(token, user))
        except requests.exceptions.HTTPError as err:
            print(err)
        return Response(status=401, data={'details': 'Could not authorize user'})