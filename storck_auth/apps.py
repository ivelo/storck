from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'storck_auth'
