from lib2to3.pgen2 import token
import uuid
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from rest_framework.authtoken.models import Token
from workspace.models import Workspace, WorkspaceToken


class CERNOIDCBackend(OIDCAuthenticationBackend):
    def create_token(self, user):
        Token.objects.get_or_create(user=user)
        pass

    def create_workspace(self, user):
        workspaces = Workspace.objects.filter(users=user)

        if len(workspaces) == 0:
            workspace_token = str(uuid.uuid4())

            workspace = Workspace.objects.create(name="Main workspace")
            WorkspaceToken.objects.create(
                workspace=workspace, name="Default token", token=workspace_token
            )

            workspace.save()
            workspace.users.add(user)
        pass

    def create_user(self, claims):
        email = self.get_email(claims)
        username = claims.get('preferred_username')
        first_name = claims.get('given_name', '')
        last_name = claims.get('family_name', '')
        return self.UserModel.objects.create_user(username=username, email=email, first_name=first_name, last_name=last_name)

    def filter_users_by_claims(self, claims):
        email = self.get_email(claims)
        if not email:
            return self.UserModel.objects.none()
        user = self.UserModel.objects.filter(email__iexact=email)
        if user:
            self.create_token(user[0])
        return user

    def get_email(self, claims):
        return claims.get('email')

    def create_user(self, claims):
        email = self.get_email(claims)
        username = claims.get('sub')
        first_name = claims.get('given_name', '')
        last_name = claims.get('family_name', '')
        user = self.UserModel.objects.create_user(username=username, email=email, first_name=first_name, last_name=last_name)
        self.create_workspace(user)
        self.create_token(user)
        return user


class JWTAuthenticationBackend(OIDCAuthenticationBackend):
    
    def authenticate(self, request, *, token):
        if token is None:
            return None
        jwt_token = request.META['HTTP_AUTHORIZATION'].split()[1]
        if jwt_token != token:
            raise ValueError("Invalid access token value! Tokens do not match!")
        payload = self.verify_token(token)
        if payload:
            user = self.get_or_create_user(token, None, None)
            Token.objects.get_or_create(user=user)
            return user
        return None