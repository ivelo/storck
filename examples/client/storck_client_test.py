#!/usr/bin/python3
from storck_client import StorckClient

storck = StorckClient()


print()
print("Workspaces")
print(storck.get_workspaces())


print()
print("Upload")
print(storck.upload_file('../exampleFile2.txt', path='some/test/path'))
print(storck.upload_file('../exampleFile3.txt', path='some/other/path'))


print()
print("Info")
result = storck.get_info(path='/some/test/path')
print(result)

fileid = result['id']

print()
print("Search")
print(storck.search(
    search_dict={'stored_path__contains':'/some/path/or/name/part'}))



print()
print("Download")
print(storck.download_file(fileid, target_path="./some_test_path'"))
