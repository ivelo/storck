import requests


# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'

def get_workspaces():
    content = requests.get(
        "http://localhost:8000/api/workspaces",
        headers={'Authorization': 'Token ' + user_token}
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    print(content.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")
    print(str(content.request.body))
    workspaces = content.json()["data"]["workspaces"]
    return workspaces

if __name__ == "__main__":
    workspaces_list = get_workspaces()
    print(workspaces_list)
