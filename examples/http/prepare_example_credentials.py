import os, sys
sys.path.append('../../')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'storck.settings.settings')
import django
django.setup()
from django.contrib.auth.models import User
from workspace.models import Workspace, WorkspaceToken
from rest_framework.authtoken.models import Token
import time

def create_example_credentials():
    """
    ATTENTION!!!
    This file is here just for the test purposes.
    THIS IS NOT THE WAY HOW THIS DATABASE WORKS.
    Do not create users, tokens or workspace tokens by hand!!
    """
    username = 'test_user'
    password = 'test_pass'
    workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
    user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'
    user = None

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = User.objects.create_user(username=username, password=password)
        token = Token.objects.create(user=user, key=user_token)
        workspace = Workspace.objects.create(name="Main workspace")
        workspace.users.add(user)
        WorkspaceToken.objects.create(workspace=workspace, name="Default token", token=workspace_token)

if __name__=="__main__":
    warning= "WARNING !!! Running this script will make changes to the database. "\
    "DO NOT RUN ON PRODUCTION!!"\
    "\nPlease wait ..."
    print(warning)
    time.sleep(5)
    if not (len(sys.argv)>1 and sys.argv[1] == '-y'):
        input('If you are sure that you want to do this, press any key.')
    create_example_credentials()
