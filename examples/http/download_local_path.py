import requests
from search import get_files_list
from pathlib import Path as P
import shutil
import json

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'

def get_file():
    storck_storage_path = (P()/'../../media').resolve()

    filename = "../dft.txt"
    p = P(filename).resolve()
    query = dict(metadata__howuploaded="local")
    query_json = json.dumps(query)

    content = requests.get(
        "http://localhost:8000/api/search",
        params= {'token': workspace_token, 'query_search':query_json},
        headers={'Authorization': 'Token ' + user_token}
    )
    print(content.json())


    file_data = content.json()["files"]
    print(file_data)

    hsh = file_data[0]['hash']
    target = P('.')/hsh
    shutil.copy(storck_storage_path/hsh[0]/hsh[1]/hsh, target)
    dcontent = ""
    with open(target) as f:
        dcontent = f.read()

    with open(p) as f:
        gcontent = f.read()

    if dcontent != gcontent:
        print(dcontent)
        print(gcontent)
        raise ValueError

    # content = requests.get("http://localhost:8000/api/info?id={}&token={}".format(file_id, workspace_token), stream=True, headers={'Authorization': 'Token ' + user_token})
    # print("--------------REQUEST (HTTP)----------------")

    # print(content.request.url)
    # print(content.request.body)
    # print(content.request.headers)
    # print()
    # print("--------------RESPONSE (JSON)----------------")
    # # print(content.json())
    # print()
    # print("--------------RESPONSE----------------")
    # print(str(content.headers))
    # print(str(content.content))
    # print("--------------------------------------")
    # print(str(content.request.body))
    # if content.status_code == 200:
    #     file = content.raw.read()
    #     return file
    # else:
    #     raise ValueError


if __name__ == "__main__":
    file = get_file()
    print(file)
