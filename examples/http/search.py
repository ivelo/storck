import requests

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'

def get_files_list():
    content = requests.get(
        "http://localhost:8000/api/search",
        params= {'token': workspace_token},
        headers={'Authorization': 'Token ' + user_token}
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    print(content.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")
    print(str(content.request.body))
    if content.status_code == 200:
        return content.json()["files"]
    else:
        raise ValueError

if __name__ == "__main__":
    files = get_files_list()
    print(files)
