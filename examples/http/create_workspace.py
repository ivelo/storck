import requests

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = '73f19f87-f741-4f53-ae50-a9272dc87ea7'
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'


def create_workspace():
    payload = {'name': 'how to create workspace'}
    req = requests.post("http://localhost:8000/api/workspace", data=payload, headers={'Authorization': 'Token ' + user_token})

    print("--------------REQUEST (HTTP)----------------")
    print(req.request.url)
    print(req.request.body)
    print(req.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    print(req.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(req.headers))
    print(str(req.content))
    print("--------------------------------------")
    print(str(req.request.body))
    if req.status_code != 200:
        raise ValueError



if __name__ == "__main__":
    create_workspace()
