import requests

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
user_token = '7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be'


def auth_user(token=user_token):
    auth_content = requests.post(
        "http://localhost:8000/api/auth",
        headers={'Authorization': 'Token ' + token}
    )
    print("--------------REQUEST (HTTP)----------------")
    print(auth_content.request.url)
    print(auth_content.request.body)
    print(auth_content.request.headers)
    print()
    print("--------------REQUEST (JSON)----------------")
    print(auth_content.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(auth_content.headers))
    print(str(auth_content.content))
    print("--------------------------------------")
    print(str(auth_content.request.body))


    return auth_content.json()['token']


if __name__ == "__main__":
    token = auth_user()
    print(token)
