import os


def get_current_git_commit():
    commit_hash = os.getenv('GIT_COMMIT_HASH')
    if not commit_hash:
        try:
            from sh.contrib import git
            commit_hash = git("rev-parse", "--short", "HEAD")
            return commit_hash
        except:
            return None
