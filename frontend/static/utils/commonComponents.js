export const createSection = ({ mainValue, subValue }) => {
    const section = document.createElement("div");
    const mainElement = document.createElement("div");
    const subElement = document.createElement("div");

    section.classList.add('section');
    mainElement.classList.add('mainSection');
    subElement.classList.add('subSection');

    mainElement.innerHTML = mainValue;
    subElement.innerHTML = subValue;

    section.appendChild(mainElement);
    section.appendChild(subElement);

    return section;
};

export const createExpandableSection = ({ mainValue, subValue, content }) => {
    const expandableSection = document.createElement("div");
    expandableSection.classList.add('expandableSection');
    expandableSection.classList.add('expandableSectionHidden');

    const mainSection = createSection({ mainValue, subValue });

    const contentSection = document.createElement("div");
    contentSection.classList.add('expandableContentHidden');
    contentSection.innerHTML = content.innerHTML || content;

    expandableSection.appendChild(mainSection);
    expandableSection.appendChild(contentSection);
    expandableSection.onclick = () => {
        if (contentSection.classList.contains('expandableContentHidden')) {
            expandableSection.classList.remove('expandableSectionHidden');
            contentSection.classList.remove('expandableContentHidden');
        } else {
            expandableSection.classList.add('expandableSectionHidden');
            contentSection.classList.add('expandableContentHidden');
        }
    };

    return expandableSection;
};
