
Quick Start Guide
======================================

.. image:: https://i.imgur.com/BcxBP8h.png


Before you start using storck as a database, there are essential points that you have to remember:

* **Assume that whatever you put inside the storck instance will not be deleted.** To forbid any data loss, there is no programmatic option of deleting the data once put into the storck.
* **Never share your user token with anyone.** The user token is essential of the same importance as your password. Whoever has your user token will be able to do the same operations as you.
* **Never put any private/restricted/secret data on storck.** Storck is meant to contain calibration and detector related data.


Additionally:

* If possible, don't put timestamps into the content of the data, but instead use metadata to store it as file description. The contents of the files will be **not searchable with storck**. So whatever information you will need to find the desired file (besides its path) must be put into the Metadata.
* If possible, transfer the files via filesystem as it is more efficient than sending the files via the HTTP server.


Storck access
---------------------------

To communicate with the storck database, you will need first to log in using :doc:`web interface<web_ui>` (or :doc:`the old web interface<old_web_ui>`).
There you will be able to retrieve the User Token and Workspace Token.


Inputting the data
-------------------------

Although there are many different parameters stored in the storck database, the most important for the primary usage of the storck database are described in the table below.

+--------------------+-----------------------------------------------+
|    File            |  The contents of the file                     |
+--------------------+-----------------------------------------------+
|    Path            |  A path that storck will store along the file |
+--------------------+-----------------------------------------------+
| Metadata (optional)|  timestamps, and anything that can be used to |
|                    |  search and index the file                    |
+--------------------+-----------------------------------------------+


So when you would like to store a file inside the storck database, you need to provide at least a file path.
Usage of the Metadata is optional but highly encouraged.

It may be a novelty to see the file and path as separate variables to some. In the inner ecosystem of storck, the file contents are separated from the file's path for efficient storage and retrieval.
In the internal database of storck, it is treated as a string variable that describes the file's contents.
If you upload a new file with the same path, then the new file will replace the previous file on that path.
The previous version of that file will be accessible and stored in storck.


Searching for the data
-----------------------------

The data retrieval is available through the web interface in a limited form.
You need to browse the visible files and pick the one to download.

The programmatic acces (:ref:`REST API<rest_api:REST API>`  and storck client)  has much more options when it comes to searching the files.
