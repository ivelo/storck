Running New instance
=================



Gitlab CI/CD
-----------------------

So the storck's entire stack relies on the docker and docker-compose.
GitLab's CI/CD deployment has been fully incorporated.
The details are in the ``.gitlab-ci.yml`` file, but the most important thing to remember here is the environment variables used in the entire repo.
As for now, the primary working instance is called staging, and I will be referring mainly to variables used in the deployment process for this stage.


* DOCKER_STAGING_HOST - the host of the deployment docker
* CA_STAGING, CLIENT_CERT_STAGING, CLIENT_KEY_STAGING - this variables are ssh keys created to communicate with the docker/swarm instance upon which storck will be deployed
* DATABASE_STAGING_URL, DB_STAGING_HOST, DB_STAGING_PASSWORD, DB_STAGING_PORT, DB_STAGING_USER - all of these variables can be used to connect to the desired database
* OIDC_RP_CLIENT_ID_STAGING, OIDC_RP_CLIENT_SECRET_STAGING - these secrets are necessary for the CERN SSO to work
* SECRET_STAGING_KEY - the secret key for Django
* STAGING_HOST - the address that will be available in the web browser


The lines used in the ``.gitlab-ci.yml`` in the ``staging`` section, especially:

.. code-block:: bash

    docker-compose -f docker-compose-with-db.yml build
    docker-compose -f docker-compose-with-db.yml down --remove-orphans
    docker-compose -f docker-compose-with-db.yml up -d --force-recreate

It can actually be used to deploy the storck anywhere using docker-compose.

Docker
----------------------

There are three compose files. One sets up database and volume for the storck with docker-compose. Another sets up only the volume without a database. And the last one only sets up the storck server.
Currently, the first one is used.

All of them use the same Dockerfile. I leave the reader the responsibility of familiarising, as the dockerfiles themselves are quite good descriptions of the deployment.

Django settings
-----------------------

Inside the docker, the actual settings files for the Django can be changed by the environment variables as seen in ``.gitlab-ci.yml``.
There are 3 settings files:

* ``settings.py``
* ``staging.py``
* ``deploy.py``

Both ``staging.py`` and ``deploy.py`` are reliant on the ``settings.py``, which carries the settings common for both deployment and staging.


Running locally
------------------------------------------------

Just for the test purposes it may be a good idea to run the project locally.
What you will need in here is additionally an instance of the postgresql server.
You can use `this description<https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04>`_ to create the postgres database with matching credentials to the database settings contained inside storck/settings/local_django_settings.py
If you are running ubuntu, you may need to install libpq-dev (`sudo apt install libpq-dev`).
You will also need to set up proper OIDC variables in the environment.
If not you can circumvent using `python manage.py createsuperuser` and then login using django admin.

Before running server you will need to do `makemigrations` and then `migrate`

.. code-block:: bash

    DJANGO_SETTINGS_MODULE="storck.settings.local_django_settings" SECRET_KEY="dsadsaldjakjdsakjdjkadsakjdasdadsadfrwq333" python manage.py runserver
