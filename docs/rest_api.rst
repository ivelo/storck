REST API
============================

Okay, so if you made it this far, you may also like to see the `examples` folder inside the storck repo.
It's full of examples of usage of the api using python and requests library.
All of those examples are run as part of the testing suite for storck.

Access
-----------------------------

Authorization
^^^^^^^^^^^^^^^^^^^

.. http:post:: /api/auth

   Authorize user

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/auth HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

        {
            "token":"7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be",
            "user_id":1,
            "email":"",
            "username":"test_user"
        }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :>json str token: authorisation token of the user
   :>json int user_id: id of the user
   :>json str email: email of the user
   :>json str username: username


Workspace
-------------------------


Create workspace
^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /api/workspace

   Creates workspace

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/workspace HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be
      Content-Type: application/x-www-form-urlencoded

      name=how+to+create+workspace


   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

        {
        "data":{
            "id":2,
            "name":"how to create workspace",
            "users":[
                {
                    "id":1,
                    "username":"test_user"
                }
            ],
            "tokens":[
                {
                    "id":2,
                    "workspace_id":2,
                    "token":"60d8b2c4-3202-4139-81e3-f58806c652d3",
                    "name":"Default token"
                }
            ]
        }
        }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :form name: name of the newly created workspace
   :>json int id: id of workspace
   :>json str name: name of workspace
   :>json arr users: array of users using this workspace
   :>json arr tokens: workspace token associated with this workspace

List workspaces
^^^^^^^^^^^^^^^^^^^^^^
.. http:post:: /api/workspaces

   Get list of workspaces

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/workspaces HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be


   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

      {
        "data":{
            "id":1,
            "workspaces":[
                {
                    "id":1,
                    "name":"Main workspace",
                    "users":[
                    {
                        "id":1,
                        "username":"test_user"
                    }
                    ],
                    "tokens":[
                    {
                        "id":1,
                        "workspace_id":1,
                        "token":"73f19f87-f741-4f53-ae50-a9272dc87ea7",
                        "name":"Default token"
                    }
                    ]
                },
                {
                    "id":2,
                    "name":"how to create workspace",
                    "users":[
                    {
                        "id":1,
                        "username":"test_user"
                    }
                    ],
                    "tokens":[
                    {
                        "id":2,
                        "workspace_id":2,
                        "token":"60d8b2c4-3202-4139-81e3-f58806c652d3",
                        "name":"Default token"
                    }
                    ]
                }
            ]
      }}

   :reqheader Authorization: optional OAuth token to authenticate
   :statuscode 200: no error
   :>json int id: id of user
   :>json arr workspaces: list of the workspaces associated with this user, for details on the content of the list reference :ref:`create workspace section <rest_api:create workspace>`

Files upload and download
---------------------------------------

Upload
^^^^^^^^^^^^^^

.. http:post:: /api/file

   File upload

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/file?token=73f19f87-f741-4f53-ae50-a9272dc87ea7 HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be
      Content-Type: multipart/form-data

      (@This probablye should be changed to json)


   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

      {
        "id": 1,
        "duplicate": false,
        "previous_version": "None"
      }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query string token: Workspace token
   :form path: file path to be stored in storck
   :form metadata: metadata to be stored along the file
   :form file: the file along it's content
   :>json int id: id of file
   :>json bool duplicate: True if file is duplicate of another file in storck
   :>json int previous_version: if previously a file existed under this path, then the id of the previous version of the file is returned

Download
^^^^^^^^^^^^^^

The best way is to get the files using file id's:

.. http:get:: /api/file

   File download by id

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/file?token=73f19f87-f741-4f53-ae50-a9272dc87ea7&id=1 HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

      Hello World

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query string token: Workspace token
   :query int id: file id

The other option is getting file by path, which will allways return the newest file under that path.


.. http:get:: /api/file

   File download by path

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/file?path=..%2FexampleFile.txt&token=73f19f87-f741-4f53-ae50-a9272dc87ea7 HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

      Hello World

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query str token: Workspace token
   :query str path: storage path


Browsing files
---------------------------------------


File info
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /api/info

   File download

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST info?id=1&token=73f19f87-f741-4f53-ae50-a9272dc87ea7 HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

        {
        "file":{
            "id":1,
            "file":"exampleFile.txt",
            "hash":"3e25960a79dbc69b674cd4ec67a72c62",
            "workspace_id":1,
            "user_id":1,
            "previous_version_id":"None",
            "duplicate_of_id":"None",
            "date":"2022-03-11T10:19:44.674709Z",
            "stored_path":"../exampleFile.txt",
            "metadata":{
                "testproperty":"testvalue"
            },
            "meta_closed":"None",
            "hide":false
        }
        }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query string token: Workspace token
   :query int id: file id
   :>json arr files: array of all of the files stored in workspace, with their details
   :>json int id: id of file
   :>json str file: uploaded filename
   :>json int workspace_id: the id of workspace
   :>json int user_id: the id of user who uploaded the file
   :>json int previous_version_id: the id of the previous version of the file
   :>json int duplicate_of_id: the id of the duplicate of the file
   :>json str date: the date of upload
   :>json str stored_path: the storage path
   :>json obj metadata: json containing the metadata
   :>json str meta_closed: close metadata
   :>json bool hide: if true this file should be hidden from viewing


Listing files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /api/search

   Listing files

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/search?token=73f19f87-f741-4f53-ae50-a9272dc87ea7 HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

        {
        "files":[
            {
                "id":1,
                "file":"exampleFile.txt",
                "hash":"3e25960a79dbc69b674cd4ec67a72c62",
                "workspace_id":1,
                "user_id":1,
                "previous_version_id":"None",
                "duplicate_of_id":"None",
                "date":"2022-03-11T10:19:44.674709Z",
                "stored_path":"../exampleFile.txt",
                "metadata":{
                    "testproperty":"testvalue"
                },
                "meta_closed":"None",
                "hide":false
            }
        ]
        }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query string token: Workspace token
   :>json arr files: array of all of the files stored in workspace, with their details (description of the details can be found in :ref:`file info <rest_api:file info>`)

Searching for files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /api/search

   Searching for files using the metadata

   **Example request**:

   .. http:example:: curl wget httpie python-requests

      POST /api/search?token=73f19f87-f741-4f53-ae50-a9272dc87ea7&query_search=%7B%22metadata%22%3A+%7B%22testproperty%22%3A+%22testvalue%22%7D%7D HTTP/1.1
      Host: example.com
      Accept: */*
      Authorization: Token 7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Vary: Accept
      Content-Type: application/json

        {
        "files":[
            {
                "id":1,
                "file":"exampleFile.txt",
                "hash":"3e25960a79dbc69b674cd4ec67a72c62",
                "workspace_id":1,
                "user_id":1,
                "previous_version_id":"None",
                "duplicate_of_id":"None",
                "date":"2022-03-11T10:19:44.674709Z",
                "stored_path":"../exampleFile.txt",
                "metadata":{
                    "testproperty":"testvalue"
                },
                "meta_closed":"None",
                "hide":false
            }
        ]
        }

   :reqheader Authorization: OAuth token to authenticate
   :statuscode 200: no error
   :query string token: Workspace token
   :query string metadata: A string containing json used to search through metadata (see :ref:`here <tutorial:how exactly metadata works>`)
   :>json arr files: array of all of the files stored in workspace, with their details (description of the details can be found in :ref:`file info <rest_api:file info>`)
