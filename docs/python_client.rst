Storck Python Client
======================

Storck is made with REST API, which allows for swift communication with any machine or programming language that supports HTTP requests.
To simplify the use of storck for end-users, we prepared a high-level python client.


Installation
-------------------------

To be able to use the client, you need to install the Storck Client package.
the package is available at the GitLab repository, separate from the main storck repo: https://gitlab.cern.ch/velo-calibration-software/storck_client

The installation is relatively easy; the best way is to use the client's repository directly:

.. code-block:: bash

    $ pip install git+https://gitlab.cern.ch/velo-calibration-software/storck_client


But PyPI also works

.. code-block:: bash

    $ pip install storck-client

Preparing credentials
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The two essential credentials needed for communication with Storck are

* User Token
* Workspace Token


User Token is used as **authentication key - so don't ever share it with anyone**.
Workspace Token is used to authenticate the access to the workspace, and it can be shared **To give access to the workspace**.

Both can be set as environment variables (STORCK_USER_TOKEN and STORCK_WORKSPACE_TOKEN) using source file:

.. code-block:: bash
    :name: env.sh

    STORCK_USER_TOKEN="7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"
    STORCK_WORKSPACE_TOKEN="73f19f87-f741-4f53-ae50-a9272dc87ea7"
    STORCK_API_HOST="http://localhost:8000"


When the block above is saved to the `env.sh` file, they can be used by simply calling `source env.sh`.
Additionally, the "STORCK_API_HOST" can be set to indicate the address of the storck service.

This is the preferred way of storing personal credentials to storck.


Usage
-----------------------

After installing the package, you can use it by importing StorckClient

Storck Client Module
^^^^^^^^^^^^^^^^^^^^^^^

In order to use the module, you'll need to import StorckClient class, and create an instance of it.
Then you can use all of the methods described in :ref:`it's documentation <python_client:Methods and classes of storck client>`.

.. code-block:: python

    from storck_client import StorckClient
    client = StorckClient()
    client.upload_file(self.upload_dir + "/" + file_path, path=file_path)


Methods and Classes of Storck Client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: storck_client
   :members:
   :undoc-members:
   :show-inheritance:
   :exclude-members: AutoUploadScript



Scripts
^^^^^^^^^^^^^^^^^^^^^^^^^

Along with the storck_client module come two scripts: storck_sync and storck_upload.
The command ``python -m storck_sync -h``, will output its usage:

.. code-block:: bash

    usage:storck_sync.py [-h] [--host API_HOST] [--user-token USER_TOKEN]
                    [--workspace-token WORKSPACE_TOKEN] [--dir AUTO_UPLOAD_DIR]

    Automatically upload files from a given directory.

    optional arguments:
    -h, --help            show this help message and exit
    --host API_HOST, -a API_HOST
                            STORCK api host
    --user-token USER_TOKEN, -u USER_TOKEN
                            STORCK user token
    --workspace-token WORKSPACE_TOKEN, -w WORKSPACE_TOKEN
                            STORCK workspace token
    --dir AUTO_UPLOAD_DIR, -d AUTO_UPLOAD_DIR
                            auto upload directory

An examplary usage: ``python -m storck_sync path/to/dir``.
This script will automatically upload files to the storck. The token and host arguments can be set using environmental variables.

The second script storck_upload can be used to upload a single file along with its metadata:

.. code-block:: bash

    usage: storck_upload.py [-h] [--storck_filepath STORCK_FILEPATH] [--metadata_str AUTO_UPLOAD_DIR] [--host API_HOST]
                            [--user-token USER_TOKEN] [--workspace-token WORKSPACE_TOKEN]
                            file

    This script uploads a single file in to the storck, along with optional metadata. !!Warning for the future!! This script creates
    just a single instance of the storck client connection, and destroys it after upload It might be more suitable in the future to
    use a mechanism that will continously wait for new uploads.

    positional arguments:
    file                  file that wil lbe uploaded

    optional arguments:
    -h, --help            show this help message and exit
    --storck_filepath STORCK_FILEPATH
                            if you want to store the ifle in storck under different path than provided
    --metadata_str AUTO_UPLOAD_DIR
                            auto upload directory
    --host API_HOST, -a API_HOST
                            STORCK api host
    --user-token USER_TOKEN, -u USER_TOKEN
                            STORCK user token
    --workspace-token WORKSPACE_TOKEN, -w WORKSPACE_TOKEN
                            STORCK workspace token

An examplary usage: ``python -m storck_upload path/to/file --metadata_str "metadata as str"``.

Crontab auto-upload
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Additionally, storck client repository contains an exemplary script that creates a cron jobs called regularly:

.. code-block:: bash

    #!/bin/bash

    STORCK_AUTO_UPLOAD_DIR=$STORCK_AUTO_UPLOAD_DIR
    STORCK_API_HOST=$STORCK_API_HOST
    STORCK_USER_TOKEN=$STORCK_USER_TOKEN
    STORCK_WORKSPACE_TOKEN=$STORCK_WORKSPACE_TOKEN

    SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

    apt-get update
    apt-get install cron
    pip install storck-client
    echo "0 */2 * * * /usr/bin/python3 -m storck_sync -a ${STORCK_API_HOST} -u ${STORCK_USER_TOKEN} -w ${STORCK_WORKSPACE_TOKEN} -d ${STORCK_AUTO_UPLOAD_DIR} >> ~/storck_auto_upload.log 2>&1" | crontab


How to use
""""""""""""""""""""
Auto upload script checks every 2 hours if new files were dropped into the directory it is watching on.
Script will upload files into the workspace set during the installation process (based on workspace token).
We can choose which instance of STORCK we want to coop with by setting proper host value during installation.

To install the script we need to set the following environment variables:

* STORCK_AUTO_UPLOAD_DIR - absolute path to the directory to watch
* STORCK_API_HOST - STORCK api host
* STORCK_USER_TOKEN - STORCK user token
* STORCK_WORKSPACE_TOKEN - STORCK workspace token


Installation
""""""""""""""""""""""""""

1. Install storck client
2. set environment variables listed above
3. run installation script bash install.sh

Check running process
""""""""""""""""""""""""""
You can check if process was correctly installed by running the command ``crontab -l``
You should get something similar to:

.. code-block:: bash

    0 */2 * * * /usr/bin/python3 -m sstorck_sync -a $STORCK_API_HOST -u $STORCK_USER_TOKEN -w $STORCK_WORKSPACE_TOKEN -d $STORCK_AUTO_UPLOAD_DIR >> ~/storck_auto_upload.log 2>&1



Logging
""""""""""""""""""""""""
To check what files were uploaded by script or if it is working correctly, you can check the logs file
``~/storck_auto_upload.log``
