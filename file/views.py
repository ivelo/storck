from pathlib import Path
import os
from django.http import HttpResponse
from django.db.models import Q
from django.core.files import File as DjangoFile
from django.conf import settings
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from rest_framework import views
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from .utils import hash_file, get_storck_file_path, hash_large_multi, hash_chunked_file
from .models import StorckFile, Node, ParentChild, NodeSerializer
from .forms import FileForm
from workspace.models import WorkspaceToken, Workspace, MetadataSchemas
import json
import jsonschema
import shutil


media_root_path = Path(settings.MEDIA_ROOT)


def _create_nodes(path, workspace, file=None):
    normalized = Path(path).as_posix()
    dirs = str(normalized).split('/')

    for i in range(len(dirs) - 1):
        parent_path = "/".join(dirs[:i + 1])
        child_path = f"{parent_path}/{dirs[i + 1]}"

        nodes = Node.objects.filter(path=child_path, workspace=workspace, file=None)
        if nodes.exists() and not (file is not None and i == len(dirs) - 2):
            continue

        if i == len(dirs) - 2:
            child_node = Node(path=child_path, workspace=workspace, file=file)
        else:
            child_node = Node(path=child_path, workspace=workspace)
        child_node.save()

        nodes = Node.objects.filter(path=parent_path, workspace=workspace, file=None)
        parent_node = nodes[0] if nodes.exists() else None
        relation = ParentChild(parent_node=parent_node, child_node=child_node)
        relation.save()


class SearchFileView(views.APIView):
    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        hidden_files = request.GET.get("hidden", False)
        queryset = None
        if hidden_files == "True":
            queryset = StorckFile.objects.filter(workspace=workspace).values()
        else:
            queryset = StorckFile.objects.filter(
                workspace=workspace, hide=False
            ).values()
        query_search = request.GET.get("query_search", None)
        if query_search:
            try:
                query_search_dict = json.loads(query_search)
                print(query_search_dict)
            except json.JSONDecodeError:
                return Response(
                    status=400, data={"message": "invalid query_search json request"}
                )
            queryset = queryset.filter(**query_search_dict)
        return Response({"files": list(queryset)})


class LocalFileError(Exception):
    pass


class MetadataEmpty(Exception):
    pass


class InfoView(views.APIView):
    parser_classes = (
        FormParser,
        MultiPartParser,
    )

    def prepare_response(self, request, data_qs):
        if data_qs is None:
            return Response(status=404, data={"message": "StorckFile not found"})
        return Response({"file": data_qs.values().first()})

    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        stored_path = request.GET.get("path")
        file_id = request.GET.get("id")
        if stored_path is None and file_id is None:
            return Response(status=400, data={"message": "missing id or path"})
        elif file_id is not None:
            data_qs = StorckFile.objects.filter(id=file_id, workspace=workspace)
        else:
            _files = StorckFile.objects.filter(
                stored_path=stored_path, workspace=workspace
            )
            if not _files:
                return Response(status=404, data={"message": "File not found"})
            else:
                data_qs = _files.order_by("-id")
        response = self.prepare_response(request, data_qs)
        return response


class FileView(views.APIView):
    parser_classes = (
        FormParser,
        MultiPartParser,
    )

    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        file_id = request.GET.get("id")
        if file_id is None:
            return Response(status=400, data={"message": "Missing file id"})
        else:
            data_qs = StorckFile.objects.filter(id=file_id, workspace=workspace)
            filemodel_object = data_qs.first()
            if filemodel_object.duplicate_of is None:
                fp = filemodel_object.file
            else:
                fp = filemodel_object.duplicate_of.file
            data = fp.open().read()
            fp.close()
            response = HttpResponse(data, content_type="application/force-download")
            filename = Path(filemodel_object.stored_path).name
            response["Content-Disposition"] = 'attachment; filename="{}"'.format(
                filename
            )
            return response

    # def depr_add_by_local(self, request):
    #     # https://stackoverflow.com/questions/3501588/how-to-assign-a-local-file-to-the-filefield-in-django
    #     # Ideally the file should not be read but only assigned, if this is not the case in new django
    #     # the workaraound is in the link above
    #     local_path = request.POST['local_path']
    #     local_file_path = Path(local_path)
    #     if local_file_path.is_file():
    #         local_hash=hash_large_multi(local_path)
    #         if settings.REQUIRE_HASH:
    #             file_hash = request.POST['hash']
    #             if file_hash!=local_hash:
    #                 #when hash does not match u0p
    #                 return Response()

    #         existing = media_root./file_hash
    #         if existing.is_file():
    #             pass
    #         else:
    #             local_file_path.rename(existing)
    #     form = FileForm(request.POST)
    #     if form.is_valid():
    #         storck_file = form.save(commit=False)
    #         new
    #     pass

    # def add_by_request(self, request):
    #     workspace_token = request.GET["token"]
    #     token = request.headers.get("Authorization").replace("Token ", "")
    #     workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
    #     token_user = Token.objects.get(key=token).user
    #     form = FileForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         new_file = self.successful_update(
    #             request=request,
    #             new_file=form.save(commit=False),
    #             token_user=token_user,
    #             workspace=workspace,
    #         )
    #         previd = (
    #             new_file.previous_version.id
    #             if new_file.previous_version is not None
    #             else None
    #         )
    #         return Response(
    #             status=200,
    #             data={
    #                 "id": new_file.id,
    #                 "duplicate": new_file.duplicate_of is not None,
    #                 "previous_version": previd,
    #             },
    #         )
    #     return Response(status=400)

    # def npost(self, request):
    #     workspace_token = request.GET.get("token", None)
    #     if workspace_token is None:
    #         return Response(status=403, data={"message": "Missing workspace token"})
    #     if request.POST.get('local', False):
    #         response = self.add_by_local(request)
    #     else:
    #         response = self.add_by_request(request)
    #     return response

    def check_duplicate(self, file_hash):
        duplicate = StorckFile.objects.filter(hash=file_hash)
        # assert duplicate.count() <= 1
        if duplicate.exists():
            duplicated_file = duplicate.order_by("id")[0]
        else:
            duplicated_file = None
        return duplicated_file

    def check_previous_version(self, stored_path, workspace):
        previous_versions = StorckFile.objects.filter(
            stored_path=stored_path, workspace=workspace
        )
        # @TODO check if it excludes current
        # previous_versions = previous_versions.exclude(id=object_id)
        last_version = previous_versions.order_by("-id").first()
        if last_version is not None:
            last_version.hide = True
            last_version.save()
        return last_version

    def process_duplicate(self, duplicated_record, uploaded_file):
        if duplicated_record is None:
            # @TODO copy a file into right place
            rfile = uploaded_file
        else:
            rfile = None
        return rfile

    def process_transaction(
        self, request, file_hash, uploaded_file, stored_path, workspace
    ):
        duplicated_record = self.check_duplicate(file_hash)
        duplicated_file = self.process_duplicate(duplicated_record, uploaded_file)
        previous_version = self.check_previous_version(stored_path, workspace)
        # metadata = request.POST.get("metadata", None)
        # if metadata:
        #     metadata = json.loads(metadata)
        duplicate_of = duplicated_file
        return duplicated_record, duplicated_file, previous_version

    def process_local_file(self, request, local_file_path: Path):
        # https://stackoverflow.com/questions/3501588/how-to-assign-a-local-file-to-the-filefield-in-django
        # Ideally the file should not be read but only assigned, if this is not the case in new django
        # the workaraound is in the link above
        if local_file_path.is_file():
            server_side_hash = hash_file(local_file_path)
            if settings.REQUIRE_HASH:
                file_hash = request.POST["hash"]
                if file_hash != server_side_hash:
                    raise LocalFileError(
                        f"given hash {file_hash} is not matching sent hash"
                    )
            target_filepath = media_root_path / file_hash
            if not target_filepath.is_file():
                shutil.copy(local_file_path, target_filepath)
            # else if file is already here, no need to do anything
            return file_hash
        else:
            msg = (
                "local file: [{}] not found, make sure that the path is absolute"
                "and storck can access the file"
            )
            txt = msg.format(local_file_path)
            raise LocalFileError(txt)

    def process_file(self, request):
        local_path = request.POST.get("local_path")
        request_file = request.FILES.get("file", None)
        path_request = request.POST.get('path')
        if request_file is not None:
            f = request_file.open("rb")
            stored_path = path_request or request_file.name
            # file_hash = hash_large_multi(f)
            file_hash = hash_chunked_file(f)
            dirname = media_root_path / file_hash[0] / file_hash[1]
            dirname.mkdir(parents="true", exist_ok="true")
            name = dirname / file_hash
            uploaded_file = DjangoFile(f, name=name)
        elif request.POST.get("local", False) and local_path:
            local_file_path = Path(local_path)
            file_hash = self.process_local_file(request, local_file_path)
            dirname = media_root_path / file_hash[0] / file_hash[1]
            dirname.mkdir(parents="true", exist_ok="true")
            name = dirname / file_hash
            shutil.move(media_root_path / file_hash, name)
            f = open(name)
            uploaded_file = DjangoFile(f, name=name)
            stored_path = path_request or local_path
        else:
            raise LocalFileError("no file specified")
        return uploaded_file, file_hash, stored_path

    def check_metadata_schema(self, request, workspace):
        metadata = request.POST.get("metadata", None)
        metadata_loaded = None
        if metadata is not None:
            metadata_loaded = json.loads(metadata)
        if (filetype := request.POST.get("filetype", None)) is not None:
            schemas = MetadataSchemas.objects.filter(
                workspace=workspace, filetype=filetype
            )
            if schemas.exists() and metadata is None:
                raise MetadataEmpty
            else:
                schema = schemas.first().schema
            jsonschema.validate(instance=metadata_loaded, schema=schema)
            return metadata_loaded
        return metadata_loaded

    @permission_classes([IsAuthenticated])
    def post(self, request):
        # form = FileForm(request.POST)
        # if form.is_valid():
        # new_file = form.save(commit=False)
        try:
            uploaded_file, file_hash, stored_path = self.process_file(request)
        except LocalFileError as e:
            return Response(status=400, data={"message": str(e)})
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            response = Response(status=403, data={"message": "Missing workspace token"})
            return response
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace
        workspace_is_users = Workspace.objects.filter(
            pk=workspace.pk, users__pk=request.user.pk
        ).exists()
        stored_path = request.POST.get("path", os.path.basename(uploaded_file.name))
        error_data = {"message": "invalid metadata"}
        if workspace_is_users:
            try:
                meta = self.check_metadata_schema(request, workspace)
            except json.JSONDecodeError:
                error_data["cause"] = "error when parsing JSON metadata"
                return Response(status=400, data=error_data)
            except jsonschema.exceptions.ValidationError as err:
                msg = str(err)
                error_data["cause"] = "invalid metadata: \n {}".format(msg)
                return Response(status=400, data=error_data)
            except MetadataEmpty:
                error_data[
                    "cause"
                ] = "metadata field empty, but this filetype requres schema"
                return Response(status=400, data=error_data)
            normalized_stored_path = Path("///" + stored_path).as_posix()
            
            duplicated_record, duplicated_file, previous = self.process_transaction(
                request, file_hash, uploaded_file, normalized_stored_path, workspace
            )
            
            new_file = StorckFile(
                user=request.user,
                file=duplicated_file,
                duplicate_of=duplicated_record,
                previous_version=previous,
                metadata=meta,
                hash=file_hash,
                stored_path=normalized_stored_path,
                workspace=workspace,
            )
            new_file.save()

            _create_nodes(normalized_stored_path, workspace, new_file)

            return Response(
                status=200,
                data={
                    "id": new_file.id,
                    "duplicate": new_file.duplicate_of is not None,
                    "previous_version": previous.id if previous is not None else None,
                },
            )
        else:
            response = Response(status=403, data={"message": "workspace access denied"})
        return response

    def successful_update(self, request, new_file, token_user, workspace):
        # @TODO this later should moved into model
        # also how to handle file before saving it in the database :
        # https://docs.djangoproject.com/en/3.0/topics/http/file-uploads/
        new_file.user_id = token_user.pk
        new_file.workspace = workspace
        new_file.save(commit=False)

        stored_path = request.POST.get("path", os.path.basename(new_file.file.name))
        metadata = request.POST.get("meta", None)

        file_path = get_storck_file_path(new_file)
        hash = hash_file(file_path)
        duplicate_of = StorckFile.objects.filter(hash=hash).first()

        if duplicate_of:
            self.handle_duplicates(
                request=request, new_file=new_file, duplicate_of=duplicate_of,
            )

            hash = duplicate_of.hash

        prev_version = self.handle_prev_versions(
            stored_path=stored_path, workspace=workspace, object_id=new_file.id,
        )

        new_file.hash = hash
        new_file.stored_path = stored_path
        new_file.metadata = metadata
        new_file.previous_version = prev_version
        new_file.save()
        return new_file

    def handle_duplicates(self, request, new_file, duplicate_of):
        new_file.file.delete()
        new_file.file = duplicate_of.file
        new_file.duplicate_of = duplicate_of

    def handle_prev_versions(self, stored_path, workspace, object_id):
        previous_versions = StorckFile.objects.filter(
            stored_path=stored_path, workspace=workspace
        )
        previous_versions = previous_versions.exclude(id=object_id)
        prev_version = previous_versions.order_by("-id").first()
        if prev_version is not None:
            prev_version.hide = True
            prev_version.save()
        return prev_version


class DirectoryView(views.APIView):
    @permission_classes([IsAuthenticated])
    def post(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace

        dir_path = request.POST.get("path", None)
        if dir_path is None:
            return Response(status=400, data={"message": "Missing file path"})

        _create_nodes(dir_path, workspace)
        return Response(status=204)

    def get(self, request):
        workspace_token = request.GET.get("token", None)
        if workspace_token is None:
            return Response(status=403, data={"message": "Missing workspace token"})
        workspace = WorkspaceToken.objects.get(token=workspace_token).workspace

        dir_path = request.GET.get("path", None)
        if dir_path is None:
            return Response(status=400, data={"message": "Missing file path"})
        dir_path = Path(dir_path).as_posix()

        response = []
        if dir_path in ('/', '', '.'):
            relations = ParentChild.objects.filter(parent_node=None)
            relations = filter(
                lambda relation: relation.child_node.workspace == workspace, relations
            )
        else:
            parent_node = Node.objects.filter(path=dir_path, workspace=workspace, file=None)
            if not parent_node.exists():
                return Response(
                    status=404, data={"message": "Directory does not exist"}
                )
            relations = ParentChild.objects.filter(parent_node=parent_node[0])

        for relation in relations:
            node = NodeSerializer(relation.child_node).data
            response.append(node)

        return Response(status=200, data={"data": response})
